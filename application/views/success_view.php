<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url()?>assets/css/style_admin_panel.css"/>
<h4>Welcome, <?=$auth[0]['Login']?> !  <a href="<?php echo base_url();?>index.php">На главную</a> </h4>


<script type="text/javascript">
function show_div(div)
{
   top_menu.style.visibility="hidden";
   bottom_menu.style.visibility="hidden";
   phones.style.visibility="hidden";
   about.style.visibility="hidden";
   footer.style.visibility="hidden";
   slider.style.visibility="hidden";
   div.style.visibility="visible";
}
function hidden_div()
{
   bottom_menu.style.visibility="hidden";
   phones.style.visibility="hidden";
   about.style.visibility="hidden";
   footer.style.visibility="hidden";
   slider.style.visibility="hidden"
}
</script>



<body onLoad=hidden_div()>
 <ul class="main-menu">
     <li><a href="JavaScript:void(0)" onClick=show_div(top_menu)>Редактирование верхнего меню</a></li>
     <li><a href="JavaScript:void(0)" onClick=show_div(bottom_menu)>Редактирование нижнего меню</a></li>
     <li><a href="JavaScript:void(0)" onClick=show_div(phones)>Редактирование номеров телефонов</a></li>
     <li><a href="JavaScript:void(0)" onClick=show_div(about)>Редактирование "Что говорят о нас клиенты?"</a></li>
     <li><a href="JavaScript:void(0)" onClick=show_div(footer)>Редактирование футера</a></li>
     <li><a href="JavaScript:void(0)" onClick=show_div(slider)>Редактирование слайдера</a></li>
 </ul>
    
    
<div id="top_menu" style ="position:absolute;top:50px;left:250px;height:90px;width:550px;">

    <H4>Редактирование верхнего меню:</H4>

<FORM action="add" method="post" >     
    <OL> 
   <?php foreach ($menu as $item):?>
        <P> 
            <INPUT type="hidden" value="<?=$item['id'];?>">
            <?=$item['title'];?>
            <A href="delete?id=<?=$item['id'];?>">Del</A>  
        </P>
   <?php endforeach;?>
    </OL>
    <INPUT type="submit" value="Добавить">
</FORM>

</div>
<div id="bottom_menu" style ="position:absolute;top:50px;left:250px;height:90px;width:550px;">
    
    
    <H4>Редактирование нижнего меню:</H4>

<FORM action="add_bottom" method="post">     
    <OL> 
   <?php foreach ($bottom_menu as $item):?>
        <P> 
            <INPUT type="hidden" value="<?=$item['id'];?>">
            <?=$item['title'];?>
            <A href="delete_bottom?id=<?=$item['id'];?>">Del</A> 
        </P>
   <?php endforeach;?>
    </OL>
     <INPUT type="submit" value="Добавить">
</FORM>
    



</div>
<div id="phones" style ="position:absolute;top:50px;left:250px;height:90px;width:550px;">
    
    
    
    <H4>Редактирование номеров телефонов:</H4>

<FORM action="add_phone" method="post">     
    <OL> 
   <?php foreach ($phone_numbers as $item):?>
        <P> 
            <INPUT type="hidden" value="<?=$item['id'];?>">
            <?=$item['code'];?>
            <?=$item['phone'];?>
            <A href="delete_phone?id=<?=$item['id'];?>">Del</A> 
        </P>
   <?php endforeach;?>
    </OL>
     <INPUT type="submit" value="Добавить">
</FORM>



</div>
<div id="about" style ="position:absolute;top:50px;left:250px;height:90px;width:1000px;">
    
    
    <H4>Редактирование "Что говорят о нас клиенты?":</H4>

 
<FORM action="save_about" method="post">
    <OL> 
   <?php foreach ($about as $item):?>
        <P> 
            <INPUT type="hidden" value="<?=$item['id'];?>" name = "id" size="10">
            <INPUT type="text" value="<?=$item['name'];?>" name="name" class="about-us-right__name" size="10">
            <INPUT type="text" value="<?=$item['position'];?>" name="position">
            <INPUT type="text" value="<?=$item['content'];?>" name="content" size="50">
            <INPUT type="text" value="<?=$item['image'];?>" name="image" size="10">
            
            <A href="delete_about?id=<?=$item['id'];?>">Del</A> 
        </P>
   <?php endforeach;?>
    </OL>
     <INPUT type="submit" value="Сохранить"> 
     <?php echo anchor('Admin/add_about', 'Добавить', 'class="link-class"') ?>
 </FORM>
    



</div>
<div id="footer" style ="position:absolute;top:50px;left:250px;height:90px;width:550px;">
    
    
    
    <H4>Редактирование футера</H4>
<FORM action="edit_footer" method="post">     
    <OL> 
   <?php foreach ($footer_info as $item):?>
   
        <P> <INPUT type="hidden" value="<?=$item['id'];?>" name="id"</P>
        <P> <INPUT type="text" value="<?=$item['address'];?>" name="address" size="50"> </P>
        <P> <INPUT type="text" value="<?=$item['home_phone'];?>" name="home_phone" size="50"> </P>
        <P> <INPUT type="text" value="<?=$item['mobile_phone'];?>" name="mobile_phone" size="50"> </P>
        <P> <INPUT type="text" value="<?=$item['mail'];?>" name="mail" size="50"> </P>
        <P> <INPUT type="text" value="<?=$item['work_time'];?>" name="work_time" size="50"> </P>
         
   <?php endforeach;?>
    </OL>
    <INPUT type="submit" value="Сохранить">
</FORM>



</div>

    
    <div id="slider" style ="position:absolute;top:50px;left:250px;height:90px;width:750px;">
    
    
    
    <H4>Редактирование слайдера</H4>
<FORM action="add_image" method="post">     
    <OL> 
        <?php foreach ($slider as $item):?>
   
        <P> <INPUT type="hidden" value="<?=$item['id'];?>" name="id"</P>
        <P> 
            <INPUT type="text" value="<?=$item['path'];?>" name="path" size="50">
            <A href="delete_image?id=<?=$item['id'];?>">Del</A>
        </P>
        
         
   <?php endforeach;?>
    </OL>
    <INPUT type="submit" value="Добавить">
</FORM>

</div>
    
</body>
        