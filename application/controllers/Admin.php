<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    
	public function index()
	{
            $this->load->view('registration_view');
	}
        
        public function index_client()
	{
            $this->load->view('registration_client_view');
	}
        public function authorization()
        {
            $this->load->model('authorization_model');
            $this->load->model('menu_model');
            
            $data['menu'] = $this->menu_model->get_menu();
            $data['bottom_menu'] = $this->menu_model->get_bottom_menu();
            $data['phone_numbers'] = $this->menu_model->get_phone_numbers();
            $data['about'] = $this->menu_model->get_about();
            $data['auth'] = $this->authorization_model->get_login_pass();
            $data['footer_info'] = $this->menu_model->get_footer_info();
            $data['slider'] = $this->menu_model->get_slider();
            
            if (isset($_POST['login']) and isset($_POST['password']))
            {
                if ($data['auth'][0]['Login'] == $_POST['login'] and $data['auth'][0]['Password'] == $_POST['password'] )
                    {
                     $this->load->view('success_view',$data);
                    }
                else
                    {
                     $this->load->view('error_authorization_view',$data);
                    }
            }
            else
            {
                $this->load->view('success_view',$data);
            }
        }
        
        public function add()
        {
            if ($_POST == null)
            {
                $this->load->view('create_menu_item_view');
            }
            else
            {
                $this->load->model('menu_model');
                
                $data['title'] = $_POST['title'];
                $this->menu_model->add_item($data);
                redirect('Admin/authorization');
            } 
        }
        
        public function delete()
        {
                $this->load->model('menu_model');
                
                $data['id'] = $_GET['id'];
                $this->menu_model->delete_item($data);
                redirect('Admin/authorization');
        }
        
         public function add_bottom()
        {
            if ($_POST == null)
            {
                $this->load->view('create_bottom_menu_item_view');
            }
            else
            {
                $this->load->model('menu_model');
                
                $data['title'] = $_POST['title'];
                $this->menu_model->add_bottom_item($data);
                redirect('Admin/authorization');
            } 
        }
        
        public function delete_bottom()
        {
                $this->load->model('menu_model');
                
                $data['id'] = $_GET['id'];
                $this->menu_model->delete_bottom_item($data);
                redirect('Admin/authorization');
        }
        
        public function add_phone() 
        {
            if ($_POST == null)
            {
                $this->load->view('create_phone_view');
            }
            else
            {
                $this->load->model('menu_model');
                
                $data['code'] = $_POST['code'];
                $data['phone'] = $_POST['phone'];
                $this->menu_model->add_phone($data);
                redirect('Admin/authorization');
            } 
        }
        
        public function delete_phone()
        {
            $this->load->model('menu_model');
                
                $data['id'] = $_GET['id'];
                $this->menu_model->delete_phone($data);
                redirect('Admin/authorization');
        }
        
        public function add_about()
        {
            if ($_POST == null)
            {
                $this->load->view('create_about_view');
            }
            else 
            {
                 $this->load->model('menu_model');
                
                $data['name'] = $_POST['name'];
                $data['position'] = $_POST['position'];
                $data['content'] = $_POST['content'];
                
                
                $this->menu_model->add_about($data);
                redirect('Admin/authorization');
            }
        }
         public function save_about()
        {
              $this->load->model('menu_model');
                
                
                $data['id'] = $this->input->post('id');
                $data['name'] = $this->input->post('name');
                $data['position'] = $this->input->post('position');
                $data['content'] = $this->input->post('content');
                $data['image'] = $this->input->post('image');
                
                $this->menu_model->save_about($data);   
             
             redirect('Admin/authorization');
        }
        
        public function delete_about()
        {
                $this->load->model('menu_model');
                
                $data['id'] = $_GET['id'];
                $this->menu_model->delete_about($data);
                redirect('Admin/authorization');
        }
        
        public function edit_footer()
        {
                $this->load->model('menu_model');
                
                
                $data['id'] = $this->input->post('id');
                $data['address'] = $this->input->post('address');
                $data['home_phone'] = $this->input->post('home_phone');
                $data['mobile_phone'] = $this->input->post('mobile_phone');
                $data['mail'] = $this->input->post('mail');
                $data['work_time'] = $this->input->post('work_time');
                
                $this->menu_model->edit_footer($data);
                redirect('Admin/authorization');
        }
        
         public function add_image()
        {
             $this->load->helper(array('form', 'url'));  
             $this->load->view('upload_form', array('error' => ' ' ));
        }
        
         public function do_upload()
        {
                $config['upload_path']          = './assets/images';
                $config['allowed_types']        = 'gif|jpg|png';
                //$config['max_size']             = 1000;
                //$config['max_width']            = 1024;
                //$config['max_height']           = 768;

                $this->load->library('upload', $config);
                
                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('upload_success', $data);
                }
                
        }
        
        public function add_image_database()
        {
            $this->load->model('menu_model');
                
            $data['path'] = 'assets/images/'.$this->input->post('name_file');
            $this->menu_model->add_image_database($data);
            redirect('Admin/authorization');  
        }
        
        public function delete_image()
        {
            $this->load->model('menu_model');
                
                $data['id'] = $_GET['id'];
                $this->menu_model->delete_image($data);
                redirect('Admin/authorization');
        }  
        
}
