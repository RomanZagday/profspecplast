<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
	public function index()
	{
            $this->load->model('menu_model');
            
            $data['menu'] = $this->menu_model->get_menu();
            $data['bottom_menu'] = $this->menu_model->get_bottom_menu();
            $data['phone_numbers'] = $this->menu_model->get_phone_numbers();
            $data['about'] = $this->menu_model->get_about();
            $data['count_about'] = $this->menu_model->get_about_count();
            $data['about_company'] = $this->menu_model->get_about_company();
            $data['footer_info'] = $this->menu_model->get_footer_info();
            $data['slider'] = $this->menu_model->get_slider();
            $data['count_images'] = $this->menu_model->get_count_images();
            
             if (isset($_POST['name_file']))
            {
                $data['name_file'] = $_POST['name_file'];
            }
            
            if (isset($_POST['login']) and isset($_POST['password']))
            {
                if ($_POST['login'] == 'client' and $_POST['password'] == 'client' )
                    {
                        $data['client'] = true;
                        $this->load->view('home_view',$data);
                    }
            }
            else
            {
                $this->load->view('home_view',$data);
            }
	}
        
        public function login()
        {
            $this->load->view('registration_client_view');
        }
         public function feedback()
        {
            if ($_POST == null)
            {
                $this->load->view('feedback_view');
            }
            else 
            {
                $email = $this->input->post('email');
                $mail = strripos($email, '@mail.');
                $gmail = strripos($email, '@gmail.');
                if (($mail == false) and ($gmail == false))
                {
                    $data['valid'] = 'Введите корректные данные';
                    $this->load->view('feedback_view',$data);
                }
                else
                {
                    echo 'Ваша заявка принята!';
                }
                return $email;
            }
        }
}