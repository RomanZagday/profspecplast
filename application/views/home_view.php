<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Профспецпласт</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">  
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url()?>assets/css/style.css"/>
    <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css"> 
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

</head>

<body> 
            
    <header>
<!--header-->
        <div class="row_header-top">
           
            <div class="container_header-top cs-max-width">
<!--header-top-->
                <ul class="header-top-menu header-top-menu_margin">
                    <?php foreach ($menu as $item):?>
                         <li class="header-top-menu__item"><?=$item['title'];?></li>
                    <?php endforeach;?>
                </ul>
                
<!--clients entrance - Bootstrap-->
                <div class="header-top-menu__clients-enter" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                    <img src="<?php echo base_url();?>/assets/images/header-top-menu__clients-enter.png" alt="" class="header-top-menu__img">
                    <div class="header-top-menu__text" onclick="location.href='index.php/Home/login'">Вход<br>для клиентов</div>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Recipient:</label>
                                        <input type="text" class="form-control" id="recipient-name">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">Message:</label>
                                        <textarea class="form-control" id="message-text"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Send message</button>
                            </div>
                        </div>
                    </div>
                </div>
<!--/clients entrance - Bootstrap-->
                
                <div class="container_header-top-right">
                    <div class="header-top-menu__work-time">
                        <div class="header-top-menu__schedule">09:00 - 20:00</div>
                        <div class="header-top-menu__day-blocks">
                            <div class="header-top-menu__day-block"></div>
                            <div class="header-top-menu__day-block"></div>
                            <div class="header-top-menu__day-block"></div>
                            <div class="header-top-menu__day-block"></div>
                            <div class="header-top-menu__day-block"></div>
                            <div class="header-top-menu__day-block header-top-menu__day-block_holidays"></div>
                            <div class="header-top-menu__day-block header-top-menu__day-block_holidays"></div>
                        </div>
                    </div>

                    <div class="header-top-menu-phones">
                        <input type="checkbox" name="header-top-menu-phones__phone-top" id="header-top-menu-phones__phone-top">
                        <label for="header-top-menu-phones__phone-top" class="header-top-menu-phones__phone-top">
                            <img src="<?php echo base_url();?>/assets/images/header-top-menu__phones.png" alt="" class="header-top-menu-phones__handset">
                            <div class="header-top-menu-phones__phone-number">
                                <div class="header-top-menu-phones__code">+375(29)</div>
                                <div class="header-top-menu-phones__numder">757-9-707</div>
                                <div class="header-top-menu-phones__underline"></div>
                            </div>
                        </label>       

                        <div class="header-top-menu-phones__all-phones">
                            <div class="header-top-menu-phones__all-phones-item">
                                <?php foreach ($phone_numbers as $item):?>
                                <img src="<?php echo base_url();?>/assets/images/header-top-menu__phones.png" alt="" class="header-top-menu-phones__handset">
                                <div class="header-top-menu-phones__phone-number">
                                    
                                    <div class="header-top-menu-phones__code"><?=$item['code'];?></div>
                                    <div class="header-top-menu-phones__numder"><?=$item['phone'];?></div>
                                    <div class="header-top-menu-phones__underline"></div>
                                </div>
                                <?php endforeach;?>
                            </div>
                        </div>
                        
                    </div>
                    
<!--call back - Bootstrap-->
                    <?php if (!isset($client)) 
                    {
                        echo '<div class="call-back call-back_margin-left" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">';
                        echo '<a href="index.php/Home/feedback">ОБРАТНЫЙ ЗВОНОК</a>';
                        echo '</div>';
                    } 
                    ?>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Recipient:</label>
                                            <input type="text" class="form-control" id="recipient-name">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="form-control-label">Message:</label>
                                            <textarea class="form-control" id="message-text"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Send message</button>
                                </div>
                            </div>
                        </div>
                    </div>
<!--/call back - Bootstrap-->
                    
                </div>    
                    
<!--/header-top-->
            </div>
<!--share-->            
            <img src="<?php echo base_url();?>/assets/images/header-top-menu__share.png" alt="" class="header-top-menu__share">          
<!--/share-->            
        </div> 
        
        <script>
/*fix row_header-top*/
            $(document).ready(function () {
              $(window).scroll(function() {
               var top = $(document).scrollTop();
               if (top <= $('.row_header-top').innerHeight()) $('.row_header-bottom').removeClass('row_header-bottom_fixed');
                    else {
                        $('.row_header-bottom').addClass('row_header-bottom_fixed');
                    }
                });
            });
            
            $(document).ready(function () {
              $(window).scroll(function() {
               var top = $(document).scrollTop();
               if (top <= $('.row_header-top').innerHeight()) $('.slider-bootstrap').removeClass('carousel_margin-top');
                    else {
                        $('.slider-bootstrap').addClass('carousel_margin-top');
                    }
                });
            });
/*/fix row_header-top*/
        </script>
        
        <div class="row_header-bottom">
            <div class="container_header-bottom cs-max-width">
<!--header-bottom-->
                <img src="<?php echo base_url();?>/assets/images/header-bottom-menu__logo.png" alt="" class="header-bottom-menu__logo">
    <!--mobile menu-->
                <input type="checkbox" name="mobile-menu" id="mobile-menu_stroke-cross">
                <div class="mobile-menu mobile-menu_positioning">
                    <label class="mobile-menu__strokes" for="mobile-menu_stroke-cross">
                        <div class="mobile-menu__stroke mobile-menu__stroke-cross1"></div>
                        <div class="mobile-menu__stroke mobile-menu__stroke-cross2"></div>
                        <div class="mobile-menu__stroke mobile-menu__stroke-cross3"></div>
                    </label>
                </div>
    <!--/mobile menu-->
                <ul class="header-bottom-menu">
                    <?php foreach ($bottom_menu as $item):?>
                         <li class="header-bottom-menu__item"><?=$item['title'];?></li>
                    <?php endforeach;?>   
                    
                </ul>
                
    <!--header-bottom__search-->
    <form class="header-bottom-menu-search" action="index.php/Search/search" method="post">
                    <input type="text" name="search">
                </form>
    <!--/header-bottom__search-->
    
<!--header-bottom-->
            </div>
        </div>  
<!--/header-->
    </header>    
    
  
<!--slider bootstrap-->
    <div id="carouselExampleControls" class="slider-bootstrap carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
    <!--one slide-->
            <div class="carousel-item carousel-item_bg active">
                <div class="container">
                    <img class="d-block img-fluid" src="<?php echo base_url();?>/assets/images/slider-thin.png" alt="First slide">
        <!--text block-->        
                    <div class="slider-text-block">
                        <div class="slider-text-block__title">
                            СКИДКА <span class="slider-text-block_twenty"> <?php if (isset($name_file)){ echo $name_file; }?>20%</span><br>НА МОНТАЖ ОКОН
                        </div>
                        <div class="slider-text-block__text">
                            Акция действует при заказе до 22.08.2017.
                        </div>
                        <div class="call-back call-back_padding-slider">
                            ВСЕ ITEM АКЦИИ
                        </div>
                    </div>
        <!--/text block-->
                </div>
            </div>
    <!--/one slide-->
    
    <!--one slide-->
    <?php for($i = 0; $i< $count_images;$i++):?>
            <div class="carousel-item carousel-item_bg">
                <div class="container">
                    <img class="d-block img-fluid" src="<?php echo base_url().$slider[$i]['path'];?>" alt="First slide">
        <!--text block-->        
                    <div class="slider-text-block">
                        <div class="slider-text-block__title">
                            СКИДКА <span class="slider-text-block_twenty">20%</span><br>НА МОНТАЖ ОКОН
                        </div>
                        <div class="slider-text-block__text">
                            Акция действует при заказе до 22.08.2017.
                        </div>
                        <div class="call-back call-back_padding-slider">
                            ВСЕ АКЦИИ
                        </div>
                    </div>
        <!--/text block-->
                </div>
            </div>
    <?php endfor;?>
    <!--/one slide-->
        </div>
        <a href="#carouselExampleControls" role="button" data-slide="prev">
            <div class="slider__arrow slider__arrow_left">
                <svg width="50px" height="150px" id="slider__arrow" viewBox="0 0 59.53 84.19">
                    <defs>
                        <rect id="SVGID_1_" y="0" width="49.9" height="93.8"/>
                    </defs>
                    <path clip-path="url(#SVGID_2_)" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7
                        C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/>
                </svg>      
            </div>
            <span class="sr-only">Previous</span>
        </a>
        <a href="#carouselExampleControls" role="button" data-slide="next">
            <div class="slider__arrow slider__arrow_right">
                <svg width="50px" height="150px" id="slider__arrow" viewBox="0 0 59.53 84.19">
                    <defs>
                        <rect id="SVGID_1_" y="0" width="49.9" height="93.8"/>
                    </defs>
                    <path clip-path="url(#SVGID_2_)" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7
                        C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/>
                </svg>                    
            </div>
            <span class="sr-only">Next</span>
        </a>
    <!--calculator and metering-->      
       
        <script>
        /*show and hide metering*/
            $(document).scroll(function () {
                var y = $(this).scrollTop();
                if (y > 100) {
                    $('.slider-side-block').fadeIn();
                } else {
                    $('.slider-side-block').fadeOut();
                }
            });
        /*/show and hide metering*/        
        </script>             
               
        <div class="slider-side-block">
        <!--calculator-->
            <div class="slider-side-block__calc" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
               <img src="<?php echo base_url();?>/assets/images/slider-side-block__calc.png">
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Recipient:</label>
                                    <input type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Message:</label>
                                    <textarea class="form-control" id="message-text"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Send message</button>
                        </div>
                    </div>
                </div>
            </div>
        <!--/calculator-->
            
        <!--metering-->
            <div class="slider-side-block__metering" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                <img src="<?php echo base_url();?>/assets/images/slider-side-block__metering.png">
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Recipient:</label>
                                    <input type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Message:</label>
                                    <textarea class="form-control" id="message-text"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Send message</button>
                        </div>
                    </div>
                </div>
            </div>
        <!--/metering-->
        </div>
    <!--/calculator and metering-->   
    </div>  
<!--/slider bootstrap-->
    <div class="row no-gutters">
<!--main-section item-->       
        <div class="main-section col-12 col-sm-6 col-lg-3">
    <!--background-->
            <div class="main-section__bg-block">
                <div class="main-section__bg"></div>
                <div class="main-section__bg-img main-section__bg-img_okna"></div>
            </div>
    <!--/background-->            
    <!--text block-->
            <div class="main-section__cont-inner">
                <div class="main-section__cont-img">
                    <img src="<?php echo base_url();?>/assets/images/main-section-item__img-okna.png" alt="" class="main-section__img">
                </div>
                <div class="main-section__cont-text">
                    <div class="main-section__text">
                        <p>ОКНА ПВХ</p>
                        <p>БАЛКОННЫЕ РАМЫ</p>
                    </div>
                </div>
            </div>
    <!--/text block-->
        </div>
<!--/main-section item-->
        
<!--main-section item-->   
        <div class="main-section col-12 col-sm-6 col-lg-3">
    <!--background-->
            <div class="main-section__bg-block">
                <div class="main-section__bg"></div>
                <div class="main-section__bg-img main-section__bg-img_dveri"></div>
            </div>
    <!--/background-->           
    <!--text block-->
            <div class="main-section__cont-inner">
                <div class="main-section__cont-img">
                    <img src="<?php echo base_url();?>/assets/images/main-section-item__img-dveri.png" alt="" class="main-section__img">
                </div>
                <div class="main-section__cont-text">
                    <div class="main-section__text">
                        <p>ДВЕРИ ВХОДНЫЕ</p>
                        <p>ДВЕРИ МЕЖКОМНАТНЫЕ</p>
                    </div>
                </div>
            </div>
    <!--/text block-->
        </div>
<!--/main-section item-->

<!--main-section item-->   
        <div class="main-section col-12 col-sm-6 col-lg-3">
    <!--background-->
            <div class="main-section__bg-block">
                <div class="main-section__bg"></div>
                <div class="main-section__bg-img main-section__bg-img_potolki"></div>
            </div>
    <!--/background-->           
    <!--text block-->
            <div class="main-section__cont-inner">
                <div class="main-section__cont-img">
                    <img src="<?php echo base_url();?>/assets/images/main-section-item__img-potolki.png" alt="" class="main-section__img">
                </div>
                <div class="main-section__cont-text">
                    <div class="main-section__text">
                        <p>НАТЯЖНЫЕ ПОТОЛКИ</p>
                    </div>
                </div>
            </div>
    <!--/text block-->
        </div>
<!--/main-section item-->
        
<!--main-section item-->   
        <div class="main-section col-12 col-sm-6 col-lg-3">
    <!--background-->
            <div class="main-section__bg-block">
                <div class="main-section__bg"></div>
                <div class="main-section__bg-img main-section__bg-img_mebel"></div>
            </div>
    <!--/background-->           
    <!--text block-->
            <div class="main-section__cont-inner">
                <div class="main-section__cont-img">
                    <img src="<?php echo base_url();?>/assets/images/main-section-item__img-mebel.png" alt="" class="main-section__img">
                </div>
                <div class="main-section__cont-text">
                    <div class="main-section__text">
                        <p>КОРПУСНАЯ МЕБЕЛЬ</p>
                    </div>
                </div>
            </div>
    <!--/text block-->
        </div>
<!--/main-section item-->
    </div>

    <div class="container">
<!--our advantages-->
        <div class="advantages__title">
            Почему Вам стоит выбрать именно нас?
        </div>
        <div class="row no-gutters">
<!--advantages blocks-->  
            <div class="advantages-block col-xs-12 col-sm-6 col-lg-3">
                <div class="advantages-block__max-width-center">
                    <div class="advantages-block__block-img">
                       <img src="<?php echo base_url();?>/assets/images/advantages-block__img-10-years.png" alt="" class="advantages-block__img"> 
                    </div>
                    <div class="advantages-block__title">
                        10 ЛЕТ НА РЫНКЕ
                    </div>
                    <div class="advantages-block__separator">
                        <div class="advantages-block__line"></div>
                    </div>
                    <div class="advantages-block__description">
                        <div class="advantages-block__description-hide">
                            Наша компания успешно работает на рынке более 10 лет. Много отзывов и все положительные.
                        </div>
                        <ul class="advantages-block__description-show">
                            <li class="advantages-block__description-show-item">
                                Точное соблюдение сроков
                            </li>
                            <li class="advantages-block__description-show-item">
                                Нет посредников в обслуживании
                            </li>
                            <li class="advantages-block__description-show-item">
                                Собственный транспорт
                            </li>
                            <li class="advantages-block__description-show-item">
                                Точный расчет стоимости
                            </li>
                        </ul>
                    </div>
                </div>    
            </div>
            <div class="advantages-block col-xs-12 col-sm-6 col-lg-3">
                <div class="advantages-block__max-width-center">   
                    <div class="advantages-block__block-img">
                       <img src="<?php echo base_url();?>/assets/images/advantages-block__img-deadlines.png" alt="" class="advantages-block__img">
                    </div>
                    <div class="advantages-block__title">
                        НЕ НАРУШАЕМ СРОКИ
                    </div>
                    <div class="advantages-block__separator">
                        <div class="advantages-block__line"></div>
                    </div>
                    <div class="advantages-block__description">
                        <div class="advantages-block__description-hide">
                            Наша компания успешно работает на рынке более 10 лет. Много отзывов и все положительные.
                        </div>
                        <ul class="advantages-block__description-show">
                            <li class="advantages-block__description-show-item">
                                Точное соблюдение сроков
                            </li>
                            <li class="advantages-block__description-show-item">
                                Нет посредников в обслуживании
                            </li>
                            <li class="advantages-block__description-show-item">
                                Собственный транспорт
                            </li>
                            <li class="advantages-block__description-show-item">
                                Точный расчет стоимости
                            </li>
                        </ul>
                    </div>
                </div>    
            </div>
            <div class="advantages-block advantages-block_top-correction col-xs-12 col-sm-6 col-lg-3">
                <div class="advantages-block__max-width-center">
                    <div class="advantages-block__block-img">
                       <img src="<?php echo base_url();?>/assets/images/advantages-block__img-garbage.png" alt="" class="advantages-block__img">
                    </div>
                    <div class="advantages-block__title">
                        НЕ ОСТАВЛЯЕМ МУСОР
                    </div>
                    <div class="advantages-block__separator">
                        <div class="advantages-block__line"></div>
                    </div>
                    <div class="advantages-block__description">
                        <div class="advantages-block__description-hide">
                            Наша компания успешно работает на рынке более 10 лет. Много отзывов и все положительные.
                        </div>
                        <ul class="advantages-block__description-show">
                            <li class="advantages-block__description-show-item">
                                Точное соблюдение сроков
                            </li>
                            <li class="advantages-block__description-show-item">
                                Нет посредников в обслуживании
                            </li>
                            <li class="advantages-block__description-show-item">
                                Собственный транспорт
                            </li>
                            <li class="advantages-block__description-show-item">
                                Точный расчет стоимости
                            </li>
                        </ul>
                    </div>
                </div>    
            </div>
            <div class="advantages-block col-xs-12 col-sm-6 col-lg-3">
                <div class="advantages-block__max-width-center">
                    <div class="advantages-block__block-img">
                        <img src="<?php echo base_url();?>/assets/images/advantages-block__img-good-work.png" alt="" class="advantages-block__img">
                    </div>
                    <div class="advantages-block__title">
                        К НАМ ВОЗВРАЩАЮТСЯ
                    </div>
                    <div class="advantages-block__separator">
                        <div class="advantages-block__line"></div>
                    </div>
                    <div class="advantages-block__description">
                        <div class="advantages-block__description-hide">
                            80% клиентов возвращаются к нам за новыми услугами и товарами
                        </div>
                        <ul class="advantages-block__description-show">
                            <li class="advantages-block__description-show-item">
                                Точное соблюдение сроков
                            </li>
                            <li class="advantages-block__description-show-item">
                                Нет посредников в обслуживании
                            </li>
                            <li class="advantages-block__description-show-item">
                                Собственный транспорт
                            </li>
                            <li class="advantages-block__description-show-item">
                                Точный расчет стоимости
                            </li>
                        </ul>
                    </div>
                </div>    
            </div>
<!--/advantages blocks-->            
        </div>   
<!--/our advantages-->
    </div>   

    <div class="team">
        <div class="team__bg"></div>
        <div class="team__container container">
<!--our team-->
            <div class="team__title">
                Наша дружная команда
            </div>
            <div class="row no-gutters">
                <div class="team-item team-item_margin col-12 col-md-6 col-lg-4">
                    <div class="team-item__img">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2bw.png" alt="" class="team-item__img-img-bw">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2colored.png" alt="" class="team-item__img-img2colored">
                    </div>
                    <div class="team-item__text-block">
                        <div class="team-item__name">александр александров</div>
                        <div class="team-item__position">директор</div>
                        <div class="team-item__overview">
                            Работает в компании с 2010 г.
                            Большой опыт в установке потолков, 
                            дверерей и прочего.
                        </div>
                    </div>
                </div>

                <div class="team-item team-item_margin col-12 col-md-6 col-lg-4">
                    <div class="team-item__img">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2bw.png" alt="" class="team-item__img-img-bw">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2colored.png" alt="" class="team-item__img-img2colored"> 
                       
                    </div>
                    <div class="team-item__text-block">
                        <div class="team-item__name">матвей матвеев</div>
                        <div class="team-item__position">сборшик</div>
                        <div class="team-item__overview">
                            Работает в компании с 2010 г.
                            Большой опыт в установке потолков, 
                            дверерей и прочего.
                        </div>
                    </div>
                </div>
                <div class="team-item team-item_margin col-12 col-md-6 col-lg-4">
                    <div class="team-item__img">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2bw.png" alt="" class="team-item__img-img-bw">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2colored.png" alt="" class="team-item__img-img2colored">
                    </div>
                    <div class="team-item__text-block">
                        <div class="team-item__name">матвей матвеев</div>
                        <div class="team-item__position">установщик</div>
                        <div class="team-item__overview">
                            Работает в компании с 2010 г.
                            Большой опыт в установке потолков, 
                            дверерей и прочего.
                        </div>
                    </div>
                </div>
                <div class="team-item team-item_margin col-12 col-md-6 col-lg-4">
                    <div class="team-item__img">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2bw.png" alt="" class="team-item__img-img-bw">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2colored.png" alt="" class="team-item__img-img2colored">
                    </div>
                    <div class="team-item__text-block">
                        <div class="team-item__name">матвей матвеев</div>
                        <div class="team-item__position">столяр</div>
                        <div class="team-item__overview">
                            Работает в компании с 2010 г.
                            Большой опыт в установке потолков, 
                            дверерей и прочего.
                        </div>
                    </div>
                </div>
                <div class="team-item team-item_margin col-12 col-md-6 col-lg-4">
                    <div class="team-item__img">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2bw.png" alt="" class="team-item__img-img-bw">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2colored.png" alt="" class="team-item__img-img2colored">
                    </div>
                    <div class="team-item__text-block">
                        <div class="team-item__name">матвей матвеев</div>
                        <div class="team-item__position">установщик</div>
                        <div class="team-item__overview">
                            Работает в компании с 2010 г.
                            Большой опыт в установке потолков, 
                            дверерей и прочего.
                        </div>
                    </div>
                </div>
                <div class="team-item team-item_margin col-12 col-md-6 col-lg-4">
                    <div class="team-item__img">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2bw.png" alt="" class="team-item__img-img-bw">
                       <img src="<?php echo base_url();?>/assets/images/team-item__img-img2colored.png" alt="" class="team-item__img-img2colored">
                    </div>
                    <div class="team-item__text-block">
                        <div class="team-item__name">матвей матвеев</div>
                        <div class="team-item__position">мастер по потолкам</div>
                        <div class="team-item__overview">
                            Работает в компании с 2010 г.
                            Большой опыт в установке потолков, 
                            дверерей и прочего.
                        </div>
                    </div>
                </div>
            </div>
<!--/our team-->
        </div> 
    </div>
   
    <div class="about-us container">
<!--about us-->
        <div class="about-us__title">
            О компании «Профспецпласт»
        </div>
        <div class="row no-gutters">
    <!--/left block-->
            <div class="about-us-left col-12 col-lg-6">
                <div class="about-us-left__title">
                    <?php echo $about_company[0]['title'] ?>
                </div>
                <div class="about-us-left__content">
                    <?php echo  $about_company[0]['content'] ?>
                </div>
                <div class="about-us-left__button">
                    узнать больше
                </div>
            </div>    
    <!--left block-->   
       
    <!--right block Bootstrap-->
            <div class="about-us-right col-12 col-lg-6">   
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php for($i = 0; $i<$count_about;$i++):?>
                        <?php if ($i==0)
                            {
                                echo '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class="active"></li>';
                            }
                            else
                            {
                                echo '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class=""></li>';
                            }?>
                        <?php endfor;?>
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="carousel-caption d-md-block about-us-right__item-block">
                                <img src="<?php echo base_url();?>/assets/images/about-us-right__quote.png" alt="" class="about-us-right__quote">
                                <div class="about-us-left__title about-us-left__title_margin-right">
                                    Что говорят о нас клиенты
                                </div>
                                
                                <div class="about-us-right__person">
                                    <div class="about-us-right__img">
                                        <img src="<?php echo base_url()?>/'.$item['image'].'" alt="" class="about-us-right__image"> 
                                    </div>
                                    <div class="about-us-right__person-about">
                                        <div class="about-us-right__name">
                                           <?=$about[0]['name'];?>
                                        </div>
                                        <div class="about-us-right__position">
                                           <?=$about[0]['position'];?>
                                        </div>
                                    </div>
                                </div>
                                <div class="about-us-left__content">
                                    <?=$about[0]['content'];?>
                                </div>
                            </div>
                        </div>
                        
                        
                        <?php for($i=1; $i<$count_about ;$i++):?>
                        <div class="carousel-item">
                            <div class="carousel-caption d-md-block about-us-right__item-block">
                                <img src="<?php echo base_url()?>/assets/images/about-us-right__quote.png" alt="" class="about-us-right__quote">
                                <div class="about-us-left__title about-us-left__title_margin-right">
                                    Что говорят о нас клиенты
                                </div>
                                
                                <div class="about-us-right__person">
                                    <div class="about-us-right__img">
                                        <img src="" alt="img"/>
                                    </div>
                                    <div class="about-us-right__person-about">
                                        <div class="about-us-right__name">
                                           <?=$about[$i]['name'];?>
                                        </div>
                                        <div class="about-us-right__position">
                                            <?=$about[$i]['position'];?>
                                        </div>
                                    </div>
                                </div>
                                <div class="about-us-left__content">
                                    <?=$about[$i]['content'];?>
                                </div>
                            
                            </div>
                             
                        </div>
                       <?php endfor;?>
 
                    </div>
  
                </div>
                <a href="#" class="about-us-right__all">Все отзывы</a>  
            </div>    
    <!--/right block Bootstrap-->
       
        </div>     
<!--/about us-->
    </div>  
   
    <div class="questions questions_margin">
        <div class="container">
<!--any questions-->
        <div class="questions__text">
            <p class="questions__row-1" style="font-size: 48px;">Остались вопросы по нашей работе?</p>
            <p class="questions__row-2" style="font-size: 48px;"><strong>Звоните прямо сейчас!</strong> +375(29) <strong>555-55-55</strong></p>
            <p class="questions__row-3" style="font-size: 24px;">Или оставьте Ваши контакты для получения профессиональной консультации</p>
        </div>
        
        <div class="questions__button" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
           <a href="index.php/Home/feedback">получить консультацию</a> 
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Recipient:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="form-control-label">Message:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Send message</button>
                    </div>
                </div>
            </div>
        </div>
        
<!--/any questions-->
        </div>
    </div> 
       
    <div class="hidden-menu hidden-menu_padding">
        <div class="container">
<!--hidden menu-->
    <!--switcher show/hide-->  
            <div data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
                <div class="hidden-menu-switcher-block">      
                    
        <!--show menu-->
                        <div class="hidden-menu-switcher hidden-menu-switcher_down">
                            <div class="hidden-menu-switcher__arrow">
                                <div class="hidden-menu-switcher__arrow-down"></div>
                            </div>
                            <div class="hidden-menu-switcher__text hidden-menu-switcher__text_show">ПОКАЗАТЬ МЕНЮ</div>
                        </div>
        <!--/show menu-->
        <!--hide menu-->
                        <div class="hidden-menu-switcher hidden-menu-switcher_up">
                            <div class="hidden-menu-switcher__arrow">
                                <div class="hidden-menu-switcher__arrow-up"></div>
                            </div>
                            <div class="hidden-menu-switcher__text">СКРЫТЬ МЕНЮ</div>
                        </div>
        <!--/hide menu-->
                    
                </div> 
            </div>
    <!--/switcher show/hide-->
            <div class="collapse" id="collapseExample">    
        <!--menu columns-->
            <!--menu items-->   
                <div class="hidden-menu-submenu row no-gutters">
                    <div class="hidden-menu-submenu-column col-sm-6 col-md-3 col-lg-2">
                        <div class="hidden-menu-submenu-column__title">компания</div>
                        <ul class="hidden-menu-submenu-column__items">
                            <li class="hidden-menu-submenu-column__item"><a href="#">О компании</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Сотрудники</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Видео</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Фотогалерея</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Отзывы</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Новости</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Вакансии</a></li>
                        </ul>
                    </div>
                    <div class="hidden-menu-submenu-column col-sm-6 col-md-3 col-lg-2">
                        <div class="hidden-menu-submenu-column__title">покупателям</div>
                        <ul class="hidden-menu-submenu-column__items">
                            <li class="hidden-menu-submenu-column__item"><a href="#">Акции</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Кредит и рассрочка</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Как купить</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Где купить</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Вопрос-ответ</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Статьи</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Оставить заявку</a></li>
                        </ul>
                    </div>
                    <div class="hidden-menu-submenu-column col-sm-6 col-md-3 col-lg-2">
                        <div class="hidden-menu-submenu-column__title">продукция</div>
                        <ul class="hidden-menu-submenu-column__items">
                            <li class="hidden-menu-submenu-column__item"><a href="#">Окна ПВХ</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Двери ПВХ</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Потолки</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Корпусная мебель</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Распродажа</a></li>
                        </ul>
                    </div>
                    <div class="hidden-menu-submenu-column col-sm-6 col-md-3 col-lg-2">
                        <div class="hidden-menu-submenu-column__title">Услуги</div>
                        <ul class="hidden-menu-submenu-column__items">
                            <li class="hidden-menu-submenu-column__item"><a href="#">Бесплатный замер</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Монтаж</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Услуга 3</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Услуга 4</a></li>
                            <li class="hidden-menu-submenu-column__item"><a href="#">Услуга 5</a></li>
                        </ul>
                    </div>
            <!--/menu items-->
            <!--form-->   
            
                    <div class="hidden-menu-submenu-column hidden-menu-submenu-column_center col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-0">
                        <div class="hidden-menu-submenu-column__title hidden-menu-submenu-column__title_after-align">оставайтесь с нами</div>
                        <div class="hidden-menu-submenu-column__form">
                            <div class="hidden-menu-submenu-column__form-title">
                                Подпишитесь на наши новости, чтобы первыми узнавать о скидках и акциях нашей компании
                            </div>
                            <form action="index.php/Home/feedback" method="">
                            <input type="text" class="hidden-menu-submenu-column__form-name" placeholder="Имя">
                            <input type="text" class="hidden-menu-submenu-column__form-email" placeholder="Email">
                            <input type="submit" class="hidden-menu-submenu-column__form-btn" value="подписаться">
                            </form>
                        </div>
                    </div>  
            <!--/form-->        
                </div>            
        <!--/menu columns--> 
            </div>
<!--/hidden menu-->
        </div>
    </div>  



    <footer class="footer">
        <div class="container">
           
           
            <div class="row no-gutters">
<!--column 1-->
                <div class="footer-logo col-8 offset-3 col-sm-6 offset-sm-0 col-lg-3">
                    <div class="footer-logo-title">
                      <img src="<?php echo base_url()?>/assets/images/footer-logo-title.png" alt="" class="header-bottom-menu__logo">  
                    </div>
                    <div class="footer-logo-text">
                        Строительные материалы<br> в Барановичах
                    </div>
                </div>
<!--/column 1-->

<!--column 2-->
                <div class="footer-contacts col-8 offset-3 col-sm-6 offset-sm-0 col-lg-3">
                    <div class="footer-contacts__row">
                        <i class="fa fa-map-marker footer-contacts__fa" aria-hidden="true"></i>
                        <div class="footer-contacts__text">
                             <?php echo $footer_info[0]['address'];?>
                        </div>
                    </div>
                    <div class="footer-contacts__row">
                        <i class="fa fa-home footer-contacts__fa" aria-hidden="true"></i>
                        <div class="footer-contacts__text">
                             <?php echo $footer_info[0]['home_phone'];?>
                        </div>
                    </div>
                    <div class="footer-contacts__row">
                        <i class="fa fa-phone footer-contacts__fa" aria-hidden="true"></i>
                        <div class="footer-contacts__text">
                            <?php echo $footer_info[0]['mobile_phone'];?>
                        </div>
                    </div>
                    <div class="footer-contacts__row">
                        <i class="fa fa-envelope footer-contacts__fa" aria-hidden="true"></i>
                        <div class="footer-contacts__text">
                            <?php echo $footer_info[0]['mail'];?>
                        </div>
                    </div>
                    <div class="footer-contacts__row">
                        <i class="fa fa-clock-o footer-contacts__fa" aria-hidden="true"></i>
                        <div class="footer-contacts__text footer-contacts__text_line-height">
                            Время работы:<br>
                            <?php echo $footer_info[0]['work_time'];?>
                          
                        </div>
                    </div>
                </div>
<!--/column 2-->

<!--column 3-->
                <div class="footer-forms col-8 offset-3 col-sm-6 offset-sm-0 col-lg-3 offset-lg-1">
                    <div class="footer-forms__link">
                        <a href="#" >Предложить идею</a>
                    </div>
                    <div class="footer-forms__link">
                        <a href="#" >Письмо руководству</a>
                    </div>
                    
                    <div class="footer-forms__button" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                        <a href="index.php/Home/feedback">Заказать бесплатный замер</a>
                    </div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Recipient:</label>
                                            <input type="text" class="form-control" id="recipient-name">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="form-control-label">Message:</label>
                                            <textarea class="form-control" id="message-text"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Send message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
<!--/column 3-->

<!--column 4-->
                <div class="footer-socials col-8 offset-3 col-sm-6 offset-sm-0 col-lg-2">
                    <ul class="footer-socials__ul">
                        <li class="footer-socials__li">
                            <svg class="footer-socials__li-svg" viewBox="-269 392.2 56.7 56.7" id="fb">
                                <path d="M-228.6,413.9h-7.6v-5c0-1.9,1.2-2.3,2.1-2.3c0.9,0,5.4,0,5.4,0v-8.3l-7.4,0c-8.2,0-10.1,6.2-10.1,10.1v5.5h-4.8v8.5h4.8
                                c0,10.9,0,24.1,0,24.1h10c0,0,0-13.3,0-24.1h6.8L-228.6,413.9z"/>
                            </svg>
                        </li>
                        <li class="footer-socials__li">
                            <svg class="footer-socials__li-svg" width="40px" viewBox="-285 408.9 24 24" id="vk">
                                <path d="M-263.5,415.9h-3.3c-0.3,0-0.5,0.2-0.7,0.4c0,0-1.3,2.4-1.7,3.2c-1.1,2.2-1.9,1.5-1.9,0.5v-3.5c0-0.6-0.5-1.1-1.1-1.1h-2.5
                                c-0.7-0.1-1.3,0.3-1.8,0.8c0,0,1.3-0.2,1.3,1.5c0,0.4,0,1.6,0,2.6c0,0.4-0.3,0.7-0.7,0.7c-0.2,0-0.4-0.1-0.6-0.2
                                c-1-1.4-1.9-2.9-2.5-4.5c-0.1-0.2-0.4-0.4-0.6-0.4c-0.7,0-2.1,0-3,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.1,0,0.1,0,0.2
                                c0.9,2.5,4.8,10.3,9.3,10.3h1.9c0.4,0,0.7-0.3,0.7-0.7V425c0-0.4,0.3-0.7,0.7-0.7c0.2,0,0.4,0.1,0.5,0.2l2.2,2.1
                                c0.2,0.2,0.5,0.3,0.7,0.3h3c1.4,0,1.4-1,0.6-1.8c-0.5-0.5-2.5-2.6-2.5-2.6c-0.3-0.4-0.4-0.9-0.1-1.3c0.6-0.8,1.7-2.2,2.1-2.8
                                C-263,417.6-262,415.9-263.5,415.9z"/>
                            </svg>
                        </li>
                        <li class="footer-socials__li">
                            <svg class="footer-socials__li-svg" width="40px" viewBox="-269 392.2 56.7 56.7" id="inst">
                                <path d="M-225.6,397H-256c-5.3,0-9.6,4.3-9.6,9.6v10.1V437c0,5.3,4.3,9.6,9.6,9.6h30.4c5.3,0,9.6-4.3,9.6-9.6v-20.3v-10.1
                                C-216,401.3-220.3,397-225.6,397z M-222.8,402.7l1.1,0v1.1v7.3l-8.4,0l0-8.4L-222.8,402.7z M-247.9,416.7c1.6-2.2,4.2-3.6,7.1-3.6
                                s5.5,1.4,7.1,3.6c1,1.4,1.7,3.2,1.7,5.1c0,4.8-3.9,8.7-8.7,8.7c-4.8,0-8.7-3.9-8.7-8.7C-249.5,419.9-248.9,418.2-247.9,416.7z
                                 M-220.8,437c0,2.6-2.1,4.8-4.8,4.8H-256c-2.6,0-4.8-2.1-4.8-4.8v-20.3h7.4c-0.6,1.6-1,3.3-1,5.1c0,7.5,6.1,13.6,13.6,13.6
                                c7.5,0,13.6-6.1,13.6-13.6c0-1.8-0.4-3.5-1-5.1h7.4V437z"/>
                            </svg>
                        </li>
                    </ul>
                </div>
<!--/column 4-->                
            </div>
            
<!--copywrite-->
            <div class="copywrite copywrite_margin-top">
                <div class="row no-gutters justify-content-between">
    <!--left block-->
                    <div class="copywrite__left col-12 col-lg-8 col-xl-7">
                        <div class="row no-gutters">
                            <div class="col">
                                <div class="row no-gutters justify-content-between">
                                    <div class="copywrite__text col-12 col-md-5">
                                        © 2017  |  Профспецпласт. Все права защищены
                                    </div>
                                    <div class="copywrite__confidential col-12 col-md-auto">
                                        <a href="#">Политика конфиденциальности</a>
                                    </div>
                                    <div class="copywrite__site-map-block col-12 col-md-2">
                                        <div class="copywrite__site-map">
                                            Карта сайта
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    <!--/left block-->
    
    <!--right block-->
                    <div class="copywrite__right col-12 col-lg-4 col-xl-3">
                        <div class="row no-gutters">
                            <div class="copywrite__developer col-6">
                                <div class="copywrite__developer_align">
                                    <div class="copywrite__developer-title">
                                        Контекстная реклама
                                    </div>
                                    <div class="copywrite__developer-logo">
                                        <img src="<?php echo base_url()?>/assets/images/copywrite__developer-logo-no-limit.png" alt="">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="copywrite__developer col-6">
                                <div class="copywrite__developer_align">
                                    <div class="copywrite__developer-title">
                                        Разработка сайта
                                    </div>
                                    <div class="copywrite__developer-logo">
                                       <img src="<?php echo base_url()?>/assets/images/copywrite__developer-logo-scroll.png" alt=""> 
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
    <!--/right block--> 
                </div>
            </div>           
<!--/copywrite-->            
        </div>   
                
<!--to-top-->       
        <script>        
            $(document).ready(function(){

                //Check to see if the window is top if not then display button
                $(window).scroll(function(){
                    if ($(this).scrollTop() > 100) {
                        $('.to-top').fadeIn();
                    } else {
                        $('.to-top').fadeOut();
                    }
                });

                //Click event to scroll to top
                $('.to-top').click(function(){
                    $('html, body').animate({scrollTop : 0},800);
                    return false;
                });

            });            
        </script>
       
        <a href="#" class="to-top">
            <i class="to-top__arrow fa fa-angle-up" aria-hidden="true"></i>
        </a>
<!--/to-top-->
                                    
    </footer>



<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>
