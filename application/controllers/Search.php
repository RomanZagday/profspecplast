<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
 
    function __construct()
        {
            parent::__construct();
            $this->load->model('menu_model');
        }	
    public function search()
	{
            $keyword = $this->input->post('search');
            $data['results'] = $this->menu_model->search($keyword);
            $this->load->view('search_view',$data);
	}
}

