<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {
    
	public function get_menu()
	{
            $query = $this->db->get('menu'); 
            return $query->result_array(); 
	}
        
        public function get_bottom_menu()
	{
            $query = $this->db->get('bottom_menu'); 
            return $query->result_array(); 
	}
   
        public function add_item($data)
	{
             $this->db->insert('menu',$data); 
	}
        
          public function add_bottom_item($data)
	{
             $this->db->insert('bottom_menu',$data); 
	}
        
        public function delete_item($id)
	{
            $query = $this->db->delete('menu',$id); 
	}
        
        public function delete_bottom_item($id)
	{
            $query = $this->db->delete('bottom_menu',$id); 
	}
        
        public function get_phone_numbers()
	{
            $query = $this->db->get('phone_numbers'); 
            return $query->result_array(); 
	}
        
        public function add_phone($data)
	{
             $this->db->insert('phone_numbers',$data); 
	}
        public function delete_phone($id)
        {
           $this->db->delete('phone_numbers',$id);
        }
        
         public function get_about()
        {
           $query = $this->db->get('about'); 
           return $query->result_array(); 
        }
        
        public function get_about_count()
        {
           return  $this->db->count_all('about'); 
        }
        public function add_about($data)
	{
             $this->db->insert('about',$data); 
	}
        public function delete_about($id)
        {
            $this->db->delete('about',$id);
        }
        
         public function get_about_company()
        {
            $query = $this->db->get('about_company'); 
            return $query->result_array(); 
        }
        function search($keyword)
        {
            $this->db->like('title',$keyword);
            $this->db->or_like('content', $keyword);

            $query = $this->db->get('about_company');
            return $query->result();   
        }
        
        public function get_footer_info()
	{
            $query = $this->db->get('footer_info'); 
            return $query->result_array(); 
	}
        
        public function edit_footer($data)
	{
            $this->db->where('id',$data['id']);
            $this->db->update('footer_info',$data);
	}
        
        public function save_about($data)
	{
            $this->db->where('id',$data['id']);
            $this->db->update('about',$data);
	}
        
        public function get_count_images()
        {
           return  $this->db->count_all('slider'); 
        }
        
        public function get_slider()
	{
            $query = $this->db->get('slider'); 
            return $query->result_array(); 
	}
        
        public function add_image_database($data)
        {
           return $this->db->insert('slider',$data); 
        }
        
        public function delete_image($id)
	{
            $this->db->delete('slider',$id); 
	}

}

